import * as PIXI from 'pixi.js';

export class Confetti {
  color = [
    0xffffff, 0xff2a5c, 0xe4ff2a, 0xa62aff, 0x3df2da, 0xff2a9c, 0x2aff9f,
  ];
  graphics: PIXI.Graphics[] = [];
  scale: number[] = [];
  xAdd: number[] = [];
  width = 300;
  height = 600;
  quantity = 40;
   container = new PIXI.Container();

  constructor(width: number, height: number, quantity: number) {
    this.width = width;
    this.height = height;
    this.quantity = quantity;

    this.initialize();
  }

  initialize() {
    for (var i = 0; i < this.quantity; i++) {
      const colorNum = i % this.color.length;
      this.scale.push((Math.floor(Math.random() * 4) + 8) / 10);
      const x = Math.floor(Math.random() * this.width) + 10;
      const y = Math.floor(Math.random() * this.height);
      this.xAdd.push(i % 2 === 0 ? 1 : -1);
      this.graphics.push(
        new PIXI.Graphics()
          .beginFill(this.color[colorNum], 1)
          .drawRect(-5, -5, 10, 10)
      );
      this.graphics[i].position.x = x;
      this.graphics[i].position.y = y;
      this.graphics[i].scale.x = this.scale[i];
      this.graphics[i].scale.y = this.scale[i];
      this.graphics[i].skew.y = (i % 10) * 0.1;
      this.graphics[i].rotation = 0.1 + (i % 5) * 0.1;

      this.container.addChild(this.graphics[i]);
    }
  }

  public getContainer() {
    return this.container;
  }

  public animate() {
    const skew = [0.02, 0.04, 0.06, 0.08, 0.1];
    const xPlus = [0.05, 0.1, 0.15, 0.2, 0.25];
    for (let i = 0; i < this.quantity; i++) {
      let y, x;
      let colorNum = i % this.color.length;
      this.graphics[i].clear();
      this.graphics[i]
        .beginFill(this.color[colorNum], 1)
        .drawRect(-5, -5, 10, 10);
      if (this.graphics[i].y > this.height) {
        y = -10;
        x = Math.floor(Math.random() * this.width) + 10;
      } else {
        y = this.graphics[i].y + 0.2 + (i % 5) * 0.1;
      }
      if (this.graphics[i].x > this.width - 10) {
        this.xAdd[i] = -1;
      } else if (this.graphics[i].x < 10) {
        this.xAdd[i] = 1;
      }
      if (this.xAdd[i] === -1) {
        x = this.graphics[i].x - xPlus[i % 5];
      } else {
        x = this.graphics[i].x + xPlus[i % 5];
      }
      this.graphics[i].scale.x = this.scale[i];
      this.graphics[i].scale.y = this.scale[i];
      this.graphics[i].position.x = x;
      this.graphics[i].position.y = y;
      this.graphics[i].skew.y += i % 2 === 0 ? skew[i % 5] : skew[i % 5] * -1;
      this.graphics[i].rotation +=
        i % 2 === 0 ? 0.01 + (i % 5) * 0.01 : 0.01 + (i % 5) * -0.01;
    }
  }
}
