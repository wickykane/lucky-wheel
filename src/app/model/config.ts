export type GameConfig = {
    baseAssets: string;
    sprites: {
      [key: string]: {
        url: string;
        x: number;
        y: number;
        width: number;
        height: number;
        rotate?: number;
      };
    };
  };