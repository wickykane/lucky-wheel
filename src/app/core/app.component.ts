import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef,
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import * as PIXI from 'pixi.js';
import { gsap } from 'gsap';
import { PixiPlugin } from 'gsap/PixiPlugin';

import { GAME_CONFIG } from '../constant/config';
import { loadTexture } from '../utils/texture';
import { getRandomInt } from '../utils/random';
import { Confetti } from '../component/confetti/index';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  @ViewChild('container') appContainer: ElementRef | null = null;
  app: PIXI.Application = new PIXI.Application({
    height: HEIGHT,
    width: WIDTH,
    transparent: true,
  });
  sliceContainer = new PIXI.Container();
  resultContainer = new PIXI.Container();
  playingContainer = new PIXI.Container();
  controls: FormGroup;
  listSlice = SLICES_MASTER;

  showingConfetti = false;
  confetti: any;

  constructor(private fb: FormBuilder) {
    this.controls = this.fb.group({
      player: ['Guest'],
      slices: this.fb.array(DEFAULT_SLICES.map((i) => this.createSlice(i))),
    });
  }

  ngOnInit() {
    document.body.appendChild(this.app.view);
  }

  ngAfterViewInit() {
    if (this.app) {
      if (this.appContainer) {
        this.appContainer.nativeElement.appendChild(this.app.view);
      }
      this.app.ticker.stop();
      this.registerPixi();
      this.registerResource();
    }
  }

  get slices() {
    return this.controls.get('slices') as FormArray;
  }

  createSlice(data?: any) {
    return this.fb.group(data || SLICES_MASTER[0]);
  }

  addSlice() {
    this.slices.push(this.createSlice());
  }

  removeSlice() {
    this.slices.removeAt(this.slices.length - 1);
  }

  refreshSlices() {
    this.initSlices(true);
  }

  getSlices() {
    return this.slices.value.map((s: any) => {
      const slice = SLICES_MASTER.find((i) => i.key === s.key);
      return slice;
    });
  }

  initConfetti() {
    this.confetti = new Confetti(WIDTH, HEIGHT, 40);
  }

  showConfetti() {
    this.resultContainer.addChild(this.confetti.getContainer());
    this.showingConfetti = true;
  }

  hideConfetti() {}

  registerResource() {
    this.app!.loader.baseUrl = GAME_CONFIG.baseAssets;
    Object.keys(GAME_CONFIG.sprites).forEach((key: string) => {
      this.app!.loader.add(key, GAME_CONFIG.sprites[key].url);
    });
    this.app!.loader.onComplete.add(() => this.initializeGame());
    this.app?.loader.load();
  }

  registerPixi() {
    gsap.registerPlugin(PixiPlugin);
    PixiPlugin.registerPIXI(PIXI);

    gsap.ticker.add(() => {
      this.app!.ticker.update();
    });
  }

  initializeGame() {
    // Create Game Object Here
    console.log('Init game');
    this.initBackground();
    this.initSlices();
    this.initText();
    this.initSpinButton();
    this.initConfetti();

    this.app!.ticker.add((delta: number) => this.gameLoop(delta));
  }

  createBgGradient() {
    // adjust it if somehow you need better quality for very very big images
    const quality = 256;
    const canvas = document.createElement('canvas');
    canvas.width = quality;
    canvas.height = 1;

    const ctx = canvas.getContext('2d');

    // use canvas2d API to create gradient
    const grd = ctx!.createLinearGradient(0, 0, quality, 0);
    grd.addColorStop(0, '#460674');
    grd.addColorStop(1, '#5e1895');

    ctx!.fillStyle = grd;
    ctx!.fillRect(0, 0, quality, 1);

    return PIXI.Texture.from(canvas);
  }

  getResourceTextureInfo(key: string) {
    return {
      url: this.app!.loader.resources[key].url,
      viewport: GAME_CONFIG.sprites[key],
    };
  }

  initBackground() {
    // 2 part background
    const bgGradientSpriteTop = new PIXI.Sprite(this.createBgGradient());
    bgGradientSpriteTop.width = WIDTH;
    bgGradientSpriteTop.height = HEIGHT / 2;

    const bgGradientSpriteBottom = new PIXI.Sprite(this.createBgGradient());
    bgGradientSpriteBottom.width = WIDTH;
    bgGradientSpriteBottom.height = HEIGHT / 2;
    bgGradientSpriteBottom.x = WIDTH;
    bgGradientSpriteBottom.y = HEIGHT / 2;
    bgGradientSpriteBottom.scale.x *= -1;

    // Playing background
    const playTexture1 = loadTexture(this.getResourceTextureInfo('playing1'));
    const playTexture2 = loadTexture(this.getResourceTextureInfo('playing2'));
    const playingSpite1 = new PIXI.Sprite(playTexture1);
    const playingSpite2 = new PIXI.Sprite(playTexture2);

    playingSpite1.anchor.set(0.5);
    playingSpite1.x = WIDTH / 2;
    playingSpite1.y = HEIGHT / 2;
    playingSpite1.scale.set(0);

    playingSpite2.anchor.set(0.5);
    playingSpite2.x = WIDTH / 2;
    playingSpite2.y = HEIGHT / 2;
    playingSpite2.scale.set(0);

    this.playingContainer.addChild(playingSpite1);
    this.playingContainer.addChild(playingSpite2);

    // Overlay
    const bgOverlayTexture = loadTexture(
      this.getResourceTextureInfo('background')
    );

    const overlay = new PIXI.Sprite(bgOverlayTexture);
    overlay.anchor.set(0.5);
    overlay.x = WIDTH / 2;
    overlay.y = HEIGHT / 2;
    overlay.scale.set(1.6);

    this.app!.stage.addChild(bgGradientSpriteTop);
    this.app!.stage.addChild(bgGradientSpriteBottom);
    this.app!.stage.addChild(this.playingContainer);
    this.app!.stage.addChild(overlay);
    this.app!.stage.addChild(this.resultContainer);
  }

  initSlices(rerender?: boolean) {
    if (rerender) {
      this.sliceContainer.removeChildren();
    }
    // Slices Config
    const sliceTextures = this.getSlices().map((x: any) => {
      const info = this.getResourceTextureInfo(x.key);
      return {
        texture: loadTexture(info),
        viewport: info.viewport,
        zIndex: x.zIndex || 0,
      };
    });

    for (let i = 0; i < sliceTextures.length; i++) {
      const _sliceContainer = new PIXI.Container();
      const sprite = new PIXI.Sprite(sliceTextures[i].texture);
      sprite.anchor.set(0.5);
      sprite.scale.set(0.7);
      if (sliceTextures[i].viewport.rotate) {
        sprite.angle = sliceTextures[i].viewport.rotate || 0;
      }

      _sliceContainer.addChild(sprite);

      _sliceContainer.x = WIDTH / 2;
      _sliceContainer.y = HEIGHT / 2;
      _sliceContainer.pivot.set(0, sprite.height + 5);
      _sliceContainer.angle = (i * 360) / sliceTextures.length;
      _sliceContainer.zIndex = sliceTextures[i].zIndex;
      this.sliceContainer.addChild(_sliceContainer);
    }

    this.sliceContainer.x = WIDTH / 2;
    this.sliceContainer.y = HEIGHT / 2;
    this.sliceContainer.pivot.set(WIDTH / 2, HEIGHT / 2);

    // Update layer order
    this.sliceContainer.children.sort((a, b) => {
      return a.zIndex - b.zIndex;
    });
    if (!rerender) {
      this.app!.stage.addChild(this.sliceContainer);
    }
  }

  gameLoop(delta: number) {
    if(this.showingConfetti && this.confetti) {
      this.confetti.animate();
    }
  }

  // Button and Interaction
  initText() {
    const style = new PIXI.TextStyle({
      fontFamily: 'Arial',
      fontSize: 25,
      fontStyle: 'italic',
      fontWeight: 'bold',
      fill: ['#ffffff', 'orange'], // gradient
      stroke: '#orange',
      strokeThickness: 2,
      dropShadow: true,
      dropShadowColor: '#000000',
      dropShadowBlur: 2,
      dropShadowAngle: Math.PI / 6,
      dropShadowDistance: 2,
      wordWrap: true,
      wordWrapWidth: WIDTH,
    });

    const richText = new PIXI.Text('Magic Wheel By WK', style);
    richText.x = WIDTH / 2;
    richText.y = 100;
    richText.width = WIDTH / 2;
    richText.anchor.set(0.5);

    this.app!.stage.addChild(richText);

    const tl = gsap.timeline({ repeat: -1 });
    tl.to(richText, {
      pixi: { scale: 1 },
      duration: 0,
      ease: 'power2.out',
    })
      .to(richText, {
        pixi: { scale: 1.1 },
        duration: 1,
        ease: 'power2.in',
      })
      .to(richText, {
        pixi: { scale: 1 },
        duration: 1,
        ease: 'power2.out',
      });
  }

  initSpinButton() {
    const spinContainerTexture = loadTexture(
      this.getResourceTextureInfo('spinContainer')
    );
    const spintButtonTexture = loadTexture(
      this.getResourceTextureInfo('spinButton')
    );
    const overlay = new PIXI.Sprite(spinContainerTexture);
    const spinButton = new PIXI.Sprite(spintButtonTexture);

    overlay.anchor.set(0.5);
    overlay.x = WIDTH / 2;
    overlay.y = HEIGHT / 2;

    spinButton.anchor.set(0.5);
    spinButton.x = WIDTH / 2;
    spinButton.y = HEIGHT / 2;
    spinButton.scale.set(0.5);
    spinButton.alpha = 0.8;
    spinButton.interactive = true;
    spinButton.buttonMode = true;

    // Spin Button Event
    spinButton.on('mouseover', () => {
      spinButton.alpha = 1;
    });
    spinButton.on('mouseout', () => {
      spinButton.alpha = 0.8;
    });

    spinButton.on('click', () => {
      // Play game
      this.playGame();
    });

    // Arrow
    const arrowTexture = loadTexture(this.getResourceTextureInfo('arrow'));
    const arrow = new PIXI.Sprite(arrowTexture);
    arrow.anchor.set(0.5);
    arrow.x = WIDTH / 2;
    arrow.y = HEIGHT / 2 - overlay.height / 2;
    arrow.scale.set(0.8);

    this.app!.stage.addChild(overlay);
    this.app!.stage.addChild(spinButton);
    this.app!.stage.addChild(arrow);
  }

  playGame() {
    this.resetGame();
    // Fetch result from API
    const result = this.getGameResult();
    console.log(result);
    this.showGamePlayingAnimation(result.angle, () => {
      this.showGameResult(result.index);
    });
  }

  getGameResult() {
    const priceIndex = getRandomInt(0, this.getSlices().length - 1);
    return {
      index: priceIndex,
      angle: (priceIndex * 360) / this.getSlices().length + 5,
    };
  }

  showGameResult(result: number) {
    this.showConfetti();
    const text = `Congrats ${this.controls.get('player')?.value}, you win ${
      this.getSlices()[result].value
    }x.`;
    const textSprite = new PIXI.Text(
      text,
      new PIXI.TextStyle({
        fontSize: 14,
        fill: '#fff',
      })
    );
    textSprite.anchor.set(0.5);
    textSprite.x = WIDTH / 2;
    textSprite.y = HEIGHT - 50;
    this.resultContainer.addChild(textSprite);
    gsap.to(textSprite, 0.1, { x: '+=20', yoyo: true, repeat: 5 });
    gsap.to(textSprite, 0.1, { x: '-=20', yoyo: true, repeat: 5 });
  }

  resetGame() {
    this.showingConfetti = false;
    if (this.resultContainer.children.length > 0) {
      this.resultContainer.removeChildren();
    }
  }

  showGamePlayingAnimation(
    angle: number,
    cb: () => void,
    round = 6,
    roundDuration = 2
  ) {
    this.sliceContainer.angle = this.sliceContainer.angle % 360;
    this.playingContainer.children[0].angle =
      this.playingContainer.children[0].angle % 360;
    this.playingContainer.children[1].angle =
      this.playingContainer.children[1].angle % 360;

    // Timeline for background
    const bgTl = gsap.timeline({ paused: true });
    bgTl
      .to(
        this.playingContainer.children[0],
        {
          pixi: {
            scale: 1,
          },
        },
        0
      )
      .to(this.playingContainer.children[0], {
        pixi: {
          scale: 5,
        },
        duration: 1,
      })
      .to(this.playingContainer.children[0], {
        pixi: {
          angle: round * 360 - angle,
        },
        duration: round * roundDuration,
      });

    bgTl
      .to(
        this.playingContainer.children[1],
        {
          pixi: {
            scale: 1,
          },
        },
        1
      )
      .to(
        this.playingContainer.children[1],
        {
          pixi: {
            scale: 5,
          },
          duration: 2,
        },
        1
      )
      .to(
        this.playingContainer.children[1],
        {
          pixi: {
            angle: round * 360 - angle,
          },
          duration: round * roundDuration,
        },
        1
      );

    bgTl.play();

    const tl = gsap.timeline({
      onComplete: () => {
        cb();
        bgTl.pause();
      },
    });

    tl.to(this.sliceContainer, {
      pixi: {
        angle: this.sliceContainer.angle,
      },
    }).to(this.sliceContainer, {
      ease: 'power2.out',
      pixi: {
        angle: round * 360 + angle,
      },
      duration: round * roundDuration,
    });
  }
}

// Constant

const WIDTH = 300;
const HEIGHT = 650;

const SLICES_MASTER = [
  {
    key: 'slice2x',
    value: 2,
    name: '2X',
    zIndex: 1,
  },
  { key: 'slice35x', value: 35, zIndex: 2, name: '35X' },
];

const DEFAULT_SLICES = [
  SLICES_MASTER[1],
  SLICES_MASTER[0],
  SLICES_MASTER[0],
  SLICES_MASTER[0],
  SLICES_MASTER[0],
  SLICES_MASTER[0],
  SLICES_MASTER[0],
  SLICES_MASTER[0],
  SLICES_MASTER[0],
  SLICES_MASTER[0],
];
