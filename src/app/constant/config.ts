import { GameConfig } from "../model/config";

export const GAME_CONFIG: GameConfig = {
  baseAssets: 'assets/',
  sprites: {
    background: {
      url: 'lucky-wheel.png',
      x: 4,
      y: 4,
      width: 590,
      height: 410,
    },
    playing1: {
      url: 'lucky-wheel.png',
      x: 626,
      y: 685,
      width: 134,
      height: 130,
    },
    playing2: {
      url: 'lucky-wheel.png',
      x: 624,
      y: 552,
      width: 134,
      height: 130,
    },
    arrow: {
      url: 'lucky-wheel.png',
      x: 767,
      y: 1347,
      width: 40,
      height: 35,
    },
    slice35x: {
      url: 'lucky-wheel.png',
      x: 322,
      y: 1079,
      width: 112,
      height: 108,
      rotate: -24,
    },
    slice2x: {
      url: 'lucky-wheel.png',
      x: 790,
      y: 547,
      width: 88,
      height: 103,
      rotate: -90,
    },
    spinContainer: {
      url: 'lucky-wheel.png',
      x: 516,
      y: 776,
      width: 111,
      height: 111,
    },
    spinButton: {
      url: 'lucky-wheel.png',
      x: 467,
      y: 1300,
      width: 57,
      height: 57,
    }
  },
};
