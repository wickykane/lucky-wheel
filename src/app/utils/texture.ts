import * as PIXI from 'pixi.js';
import { ViewPort } from '../model/viewport';

export const loadTexture = ({
  url,
  viewport,
}: {
  url: string;
  viewport?: ViewPort;
}): PIXI.Texture => {
  const baseTexture = new PIXI.BaseTexture(url);

  if (viewport) {
    return new PIXI.Texture(
      baseTexture,
      new PIXI.Rectangle(
        viewport.x,
        viewport.y,
        viewport.width,
        viewport.height
      )
    );
  } else {
    return new PIXI.Texture(baseTexture);
  }
};
